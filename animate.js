$('.checkbox-btn').click(function(){
    $(this).toggleClass('checkbox-on');
    if ($(this).hasClass("checkbox-on")) {
      $(this).trigger("on.checkbox");
    } else {
      $(this).trigger("off.checkbox");
    }
});